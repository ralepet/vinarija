INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');


INSERT INTO vinarija (id, godina_osnivanja, ime) VALUES (1, 1960, 'Vinarija Zvonko Bogdan');
INSERT INTO vinarija (id, godina_osnivanja, ime) VALUES (2, 1970, 'Vinarija Kovacevic');
INSERT INTO vinarija (id, godina_osnivanja, ime) VALUES (3, 1960, 'Vinarija Radovanovic');

INSERT INTO tip_vina (id, ime) VALUES (1, 'Suvo belo vino');
INSERT INTO tip_vina (id, ime) VALUES (2, 'Suvo crveno vino');
INSERT INTO tip_vina (id, ime) VALUES (3, 'Suvo roze vino');


INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (1, 11, 1335.0, 2011, '8 tamburasa', 'Lepo belo vino', 1, 1);
			
INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (2, 21, 1435.0, 2015, 'Fancy crno vino', 'Lepo crno vino', 2, 2);

INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (3, 21, 1435.0, 2015, 'Fancy belo vino', 'Extra belo vino', 1, 2);

INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (4, 21, 1435.0, 2016, 'Fancy roze vino', 'Lepo roze vino', 3, 3);

INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (5, 15, 16435.0, 2016, 'Extra roze vino', 'Jos lepse roze vino', 3, 2);

INSERT INTO vino (id, broj_dostupnih_flasa, cena_flase, godina_proizvodnje, ime, opis, tip_vina_id, vinarija_id) 
VALUES (6, 16, 2435.0, 2016, 'Premium roze vino', 'Najbolje roze vino', 3, 2);


