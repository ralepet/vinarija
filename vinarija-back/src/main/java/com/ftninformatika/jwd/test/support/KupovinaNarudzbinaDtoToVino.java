package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.KupovinaNaruzbinaDto;
import com.ftninformatika.jwd.test.model.Vino;
import com.ftninformatika.jwd.test.service.VinoService;

@Component
public class KupovinaNarudzbinaDtoToVino implements Converter<KupovinaNaruzbinaDto, Vino>{
	
	@Autowired
	private VinoService vinoService;
	
	@Override
	public Vino convert(KupovinaNaruzbinaDto dto) {
		Vino entity;
		if (dto.getId() == null) {
			entity = new Vino();
		} else {
			entity = vinoService.findOneById(dto.getId());
		}
		
		return entity;
	}

}
