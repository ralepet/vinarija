package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class KupovinaNaruzbinaDto {
	
	@NotNull
	@Positive
	private Long id;
	
	@NotNull
	@Positive
	private int brojZaPromenu;

	public KupovinaNaruzbinaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrojZaPromenu() {
		return brojZaPromenu;
	}

	public void setBrojZaPromenu(int brojZaPromenu) {
		this.brojZaPromenu = brojZaPromenu;
	}
	
	
}
