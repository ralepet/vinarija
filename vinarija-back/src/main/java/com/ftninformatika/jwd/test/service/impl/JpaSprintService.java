package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Sprint;
import com.ftninformatika.jwd.test.model.Zadatak;
import com.ftninformatika.jwd.test.repository.SprintRepository;
import com.ftninformatika.jwd.test.service.SprintService;

@Service
public class JpaSprintService implements SprintService{
	
	@Autowired
	private SprintRepository sprintRepository;

	@Override
	public Sprint findOneById(Long id) {
		return sprintRepository.findOneById(id);
	}
	
	@Override
	public Sprint findOneByIme(String ime) {
		return sprintRepository.findOneByIme(ime);
	}

	@Override
	public List<Sprint> findAll() {
		return sprintRepository.findAll();
	}

	@Override
	public Sprint save(Sprint sprint) {
		return sprintRepository.save(sprint);
	}

	@Override
	public int getUkupanBrojBodova(Sprint sprint) {
		int bodovi = 0;
		for(Zadatak zadatak : sprint.getZadaci()) {
			bodovi += zadatak.getBodovi();
		}
		return bodovi;
	}


	

}
