package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.TipVina;
import com.ftninformatika.jwd.test.repository.TipVinaRepository;
import com.ftninformatika.jwd.test.service.TipVinaService;

@Service
public class JpaTipVinaService implements TipVinaService{
	
	@Autowired
	private TipVinaRepository tipVinaRepository;
	
	@Override
	public TipVina findOneById(Long id) {
		return tipVinaRepository.findOneById(id);
	}

	@Override
	public List<TipVina> findAll() {
		return tipVinaRepository.findAll();
	}

}
