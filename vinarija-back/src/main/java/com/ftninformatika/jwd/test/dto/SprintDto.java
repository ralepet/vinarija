package com.ftninformatika.jwd.test.dto;

public class SprintDto {
	
	private Long id;
	
	private String ime;
	
	private String ukupnoBodova;

	public SprintDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getUkupnoBodova() {
		return ukupnoBodova;
	}

	public void setUkupnoBodova(String ukupnoBodova) {
		this.ukupnoBodova = ukupnoBodova;
	}
	
	

}
