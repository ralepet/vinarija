package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.VinarijaDto;
import com.ftninformatika.jwd.test.model.Vinarija;

@Component
public class VinarijaToDto implements Converter<Vinarija, VinarijaDto>{

	@Override
	public VinarijaDto convert(Vinarija source) {
		VinarijaDto dto = new VinarijaDto();
		dto.setId(source.getId());
		dto.setIme(source.getIme());
		dto.setGodinaOsnivanja(source.getGodinaOsnivanja());
		return dto;
	}
	
	public List<VinarijaDto> convert(List<Vinarija>list){
		List<VinarijaDto>dto = new ArrayList<VinarijaDto>();
		for(Vinarija vinarija : list) {  
			dto.add(convert(vinarija)); 
		}
		return dto;
	}

}
