package com.ftninformatika.jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.TipVina;

@Repository
public interface TipVinaRepository extends JpaRepository<TipVina, Long>{
	
	TipVina findOneById(Long id);

}
