package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Zadatak;

public interface ZadatakService {
	
	Zadatak findOneById(Long id);

	Zadatak save(Zadatak zadatak); 

	Zadatak update(Zadatak zadatak);

	Zadatak delete(Long id);
	
	// ovaj delete mi treba da bih mogao da oduzmem bodove
	void delete(Zadatak zadatak);

	Page<Zadatak> findAll(Integer pageNo);

	List<Zadatak> findAll();
	
	Page<Zadatak> find(String imeZadatka, String imeSprinta, Integer pageNo);

}
