package com.ftninformatika.jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Sprint;

@Repository
public interface SprintRepository extends JpaRepository<Sprint, Long>{
	
	Sprint findOneById(Long id);
	
	Sprint findOneByIme(String ime);
}
