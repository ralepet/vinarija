package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.TipVinaDto;
import com.ftninformatika.jwd.test.model.TipVina;
import com.ftninformatika.jwd.test.service.TipVinaService;
import com.ftninformatika.jwd.test.support.TipVinaToDto;

@RestController
@RequestMapping(value = "/api/tipovi_vina", produces = MediaType.APPLICATION_JSON_VALUE)
public class TipVinaController {
	

	@Autowired
	private TipVinaService tipVinaService;
	
	
	@Autowired
	private TipVinaToDto toDto;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TipVinaDto>> getAll() {

		List<TipVina> list = tipVinaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
