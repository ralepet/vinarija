package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ZadatakDto {
	
	private Long id;
	
	@NotNull(message = "Nisu zadati bodovi.")
	@Min(value = 0, message = "Minimalan broj bodova je 0.")
    @Max( value = 20, message = "Maksimalan broj bodova je 20.")
	private int bodovi;

	@NotBlank(message = "Ime zadatka mora biti uneseno i ne sme biti prazan string.")
	@Size(max = 40, message = "Ime zadatka moze imati max 40 karaktera.")
	private String ime;
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long sprintId;

	private String sprintIme; 
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long stanjeId;
	
	private String stanjeIme;
	
	private String zaduzeni;

	public ZadatakDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Long getSprintId() {
		return sprintId;
	}

	public void setSprintId(Long sprintId) {
		this.sprintId = sprintId;
	}

	
	public Long getStanjeId() {
		return stanjeId;
	}

	public void setStanjeId(Long stanjeId) {
		this.stanjeId = stanjeId;
	}

	public String getZaduzeni() {
		return zaduzeni;
	}

	public void setZaduzeni(String zaduzeni) {
		this.zaduzeni = zaduzeni;
	}

	public String getSprintIme() {
		return sprintIme;
	}

	public void setSprintIme(String sprintIme) {
		this.sprintIme = sprintIme;
	}

	public String getStanjeIme() {
		return stanjeIme;
	}

	public void setStanjeIme(String stanjeIme) {
		this.stanjeIme = stanjeIme;
	}

	@Override
	public String toString() {
		return "ZadatakDto [id=" + id + ", bodovi=" + bodovi + ", ime=" + ime + ", sprintId=" + sprintId
				+ ", sprintIme=" + sprintIme + ", stanjeId=" + stanjeId + ", stanjeIme=" + stanjeIme + ", zaduzeni="
				+ zaduzeni + "]";
	}

	
	

}
