package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.VinoDto;
import com.ftninformatika.jwd.test.model.Vino;
import com.ftninformatika.jwd.test.service.TipVinaService;
import com.ftninformatika.jwd.test.service.VinarijaService;
import com.ftninformatika.jwd.test.service.VinoService;

@Component
public class DtoToVino implements Converter<VinoDto, Vino>{
	
	@Autowired
	private VinoService vinoServie;
	
	@Autowired
	private TipVinaService tipVinaService;
	
	@Autowired
	private VinarijaService vinarijaService;

	@Override
	public Vino convert(VinoDto dto) {
		Vino entity;
		if (dto.getId() == null) {
			entity = new Vino();
		} else {
			entity = vinoServie.findOneById(dto.getId());
		}
		
		if(entity != null) {
			entity.setGodinaProizvodnje(dto.getGodinaProizvodnje());
			entity.setOpis(dto.getOpisVina());
			entity.setCenaFlase(dto.getCenaFlase());
			entity.setIme(dto.getIme());
			
			if(dto.getBrojDostupnihFlasa() == null) {
				entity.setBrojDostupnihFlasa(0); // ukoliko ne posalje broj flasa, znaci da kreira novo vino, gde ce broj flasa biti 0
			} else {
				entity.setBrojDostupnihFlasa(dto.getBrojDostupnihFlasa());
			}
			
			if(dto.getIdTipaVina() != null && tipVinaService.findOneById(dto.getIdTipaVina()) != null) {
				entity.setTipVina(tipVinaService.findOneById(dto.getIdTipaVina()));
			}
			
			if(dto.getIdVinarije() != null && vinarijaService.findOneById(dto.getIdVinarije()) != null) {
				entity.setVinarija(vinarijaService.findOneById(dto.getIdVinarije()));
			}
			
		}	
		
		return entity;
	}

}
