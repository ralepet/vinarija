package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.StanjeDto;
import com.ftninformatika.jwd.test.model.Stanje;
import com.ftninformatika.jwd.test.service.StanjeService;
//import com.ftninformatika.jwd.test.support.DtoToStanje;
import com.ftninformatika.jwd.test.support.StanjeToDto;

@RestController
@RequestMapping(value = "/api/stanja", produces = MediaType.APPLICATION_JSON_VALUE)
public class StanjeContoller {
	
	@Autowired
	private StanjeService stanjeService;
	
	@Autowired
	private StanjeToDto toDto;
	
//	@Autowired
//	private DtoToStanje toStanje;
	
	@GetMapping
	public ResponseEntity<List<StanjeDto>> getAll() {

		List<Stanje> list = stanjeService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
