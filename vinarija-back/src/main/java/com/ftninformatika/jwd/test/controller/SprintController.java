package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.SprintDto;
import com.ftninformatika.jwd.test.model.Sprint;
import com.ftninformatika.jwd.test.service.SprintService;
//import com.ftninformatika.jwd.test.support.DtoToSprint;
import com.ftninformatika.jwd.test.support.SprintToDto;

@RestController
@RequestMapping(value = "/api/sprintovi", produces = MediaType.APPLICATION_JSON_VALUE)
public class SprintController {
	
	@Autowired
	private SprintService sprintService;
	
	@Autowired
	private SprintToDto toDto;
	
//	@Autowired
//	private DtoToSprint toSprint;
	

	@GetMapping
	public ResponseEntity<List<SprintDto>> getAll() {

		List<Sprint> list = sprintService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
