package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.ZadatakDto;
import com.ftninformatika.jwd.test.model.Zadatak;

@Component
public class ZadatakToDto implements Converter<Zadatak, ZadatakDto>{

	@Override
	public ZadatakDto convert(Zadatak zadatak) {
		
		ZadatakDto dto = new ZadatakDto();
		dto.setId(zadatak.getId());
		dto.setBodovi(zadatak.getBodovi());
		dto.setIme(zadatak.getIme());
		dto.setZaduzeni(zadatak.getZaduzeni());
		dto.setSprintId(zadatak.getSprint().getId());
		dto.setSprintIme(zadatak.getSprint().getIme());
		dto.setStanjeId(zadatak.getStanje().getId());
		dto.setStanjeIme(zadatak.getStanje().getIme());
		return dto;
	}
	
	public List<ZadatakDto> convert(List<Zadatak>list){
		List<ZadatakDto>dto = new ArrayList<>();
		for(Zadatak zadatak : list) {  
			dto.add(convert(zadatak)); 
		}
		return dto;
	}

}
