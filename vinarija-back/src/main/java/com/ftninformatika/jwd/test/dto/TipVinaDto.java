package com.ftninformatika.jwd.test.dto;

public class TipVinaDto {
	
	private Long id;

	private String ime;

	public TipVinaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}
	
	

}
