package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class VinoDto {
	
	private Long id;
	
	@NotBlank
	private String ime;
	
	@Size(max = 60, message = "Opis ne sme imati vise od 120 karaktera")
	private String opisVina;
	
	@NotNull
	@Positive
	private double cenaFlase;
	
	@NotNull
	@Positive
	private Integer godinaProizvodnje;
	
	private Long idVinarije;
	
	private String imeVinarije;
	
	private Long idTipaVina;
	
	private String imeTipaVina;
	
	private Integer brojDostupnihFlasa;

	public VinoDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOpisVina() {
		return opisVina;
	}

	public void setOpisVina(String opisVina) {
		this.opisVina = opisVina;
	}

	public double getCenaFlase() {
		return cenaFlase;
	}

	public void setCenaFlase(double cenaFlase) {
		this.cenaFlase = cenaFlase;
	}

	public Integer getGodinaProizvodnje() {
		return godinaProizvodnje;
	}

	public void setGodinaProizvodnje(Integer godinaProizvodnje) {
		this.godinaProizvodnje = godinaProizvodnje;
	}

	public Long getIdVinarije() {
		return idVinarije;
	}

	public void setIdVinarije(Long idVinarije) {
		this.idVinarije = idVinarije;
	}

	public String getImeVinarije() {
		return imeVinarije;
	}

	public void setImeVinarije(String imeVinarije) {
		this.imeVinarije = imeVinarije;
	}

	public Long getIdTipaVina() {
		return idTipaVina;
	}

	public void setIdTipaVina(Long idTipaVina) {
		this.idTipaVina = idTipaVina;
	}

	public String getImeTipaVina() {
		return imeTipaVina;
	}

	public void setImeTipaVina(String imeTipaVina) {
		this.imeTipaVina = imeTipaVina;
	}

	public Integer getBrojDostupnihFlasa() {
		return brojDostupnihFlasa;
	}

	public void setBrojDostupnihFlasa(Integer brojDostupnihFlasa) {
		this.brojDostupnihFlasa = brojDostupnihFlasa;
	}
	
	
}
