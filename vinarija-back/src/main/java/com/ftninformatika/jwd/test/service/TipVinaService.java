package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.TipVina;

public interface TipVinaService {
	
	TipVina findOneById(Long id);

	List<TipVina> findAll();

}
