package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Vinarija;
import com.ftninformatika.jwd.test.repository.VinarijaRepository;
import com.ftninformatika.jwd.test.service.VinarijaService;

@Service
public class JpaVinarijaService implements VinarijaService{
	
	@Autowired
	private VinarijaRepository vinarijaRepository;

	@Override
	public Vinarija findOneById(Long id) {
		return vinarijaRepository.findOneById(id);
	}

	@Override
	public List<Vinarija> findAll() {
		return vinarijaRepository.findAll();
	}

}
