package com.ftninformatika.jwd.test.dto;

public class VinarijaDto {
	
	private Long id;
	
	private String ime;
	
	private Integer godinaOsnivanja;

	public VinarijaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Integer getGodinaOsnivanja() {
		return godinaOsnivanja;
	}

	public void setGodinaOsnivanja(Integer godinaOsnivanja) {
		this.godinaOsnivanja = godinaOsnivanja;
	}
	
	

}
