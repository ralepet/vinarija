package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Vinarija;

public interface VinarijaService {
	
	Vinarija findOneById(Long id);

	List<Vinarija> findAll();

}
