package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.VinoDto;
import com.ftninformatika.jwd.test.model.Vino;

@Component
public class VinoToDto implements Converter<Vino, VinoDto>{

	@Override
	public VinoDto convert(Vino source) {
		VinoDto dto = new VinoDto();
		dto.setId(source.getId());
		dto.setCenaFlase(source.getCenaFlase());
		dto.setOpisVina(source.getOpis());
		dto.setIme(source.getIme());
		dto.setBrojDostupnihFlasa(source.getBrojDostupnihFlasa());
		dto.setGodinaProizvodnje(source.getGodinaProizvodnje());
		
		if(source.getTipVina() != null) {
		dto.setIdTipaVina(source.getTipVina().getId());
		dto.setImeTipaVina(source.getTipVina().getIme());
		}
		
		if (source.getVinarija() != null) {
			dto.setIdVinarije(source.getVinarija().getId());
			dto.setImeVinarije(source.getVinarija().getIme());
		}
		
		return dto;
	}
	

	public List<VinoDto> convert(List<Vino>list){
		List<VinoDto>dto = new ArrayList<>();
		for(Vino vino : list) {   
			dto.add(convert(vino));
		}
		return dto;
	}

}
