package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Vino;
import com.ftninformatika.jwd.test.repository.VinoRepository;
import com.ftninformatika.jwd.test.service.VinoService;

@Service
public class JpaVinoService implements VinoService{
	
	@Autowired
	private VinoRepository vinoRepository;

	@Override
	public Vino findOneById(Long id) {
		return vinoRepository.findOneById(id);
	}

	@Override
	public Vino save(Vino vino) {
		return vinoRepository.save(vino);
	}

	@Override
	public Vino update(Vino vino) {
		return vinoRepository.save(vino);
	}

	@Override
	public Vino delete(Long id) {
		Optional<Vino> vio = vinoRepository.findById(id);
		if (vio.isPresent()) {
			vinoRepository.deleteById(id);
			return vio.get();
		}
		return null;
	}

	@Override
	public Page<Vino> findAll(Integer pageNo) {
		return vinoRepository.findAll(PageRequest.of(pageNo, 3));
	}

	@Override
	public List<Vino> findAll() {
		return vinoRepository.findAll();
	}

	@Override
	public Page<Vino> find(String ime, Long vinarijaId, Integer pageNo) {
		
		if(ime == null) {
			ime = ""; // posto je prazan
		}
		
		if(vinarijaId == null) {
			return vinoRepository.findByImeIgnoreCaseContains(ime, PageRequest.of(pageNo, 3));
		}
		
		return vinoRepository.findByImeIgnoreCaseContainsAndVinarijaId(ime, vinarijaId, PageRequest.of(pageNo, 3));
	}

	@Override
	public Vino naruciVino(Vino vino, int brojNarucenih) {
		vino.setBrojDostupnihFlasa( vino.getBrojDostupnihFlasa() + brojNarucenih); // dodamo broj narucenih
		vinoRepository.save(vino); // sacuvamo izmene
		return vino;
	}

	@Override
	public Vino kupiVino(Vino vino, int brojKupljenih) {
		vino.setBrojDostupnihFlasa( vino.getBrojDostupnihFlasa() - brojKupljenih);
		vinoRepository.save(vino);
		return vino;
	}

}
