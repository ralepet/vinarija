package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.KupovinaNaruzbinaDto;
import com.ftninformatika.jwd.test.dto.VinoDto;
import com.ftninformatika.jwd.test.model.Vino;
import com.ftninformatika.jwd.test.service.VinoService;
import com.ftninformatika.jwd.test.support.DtoToVino;
import com.ftninformatika.jwd.test.support.KupovinaNarudzbinaDtoToVino;
import com.ftninformatika.jwd.test.support.VinoToDto;

@RestController
@RequestMapping(value = "/api/vina", produces = MediaType.APPLICATION_JSON_VALUE)
public class VinoController {
	
	
	@Autowired
	private VinoService vinoService;
	
	@Autowired
	private VinoToDto toDto;
	
	@Autowired
	private DtoToVino toVino;
	
	@Autowired
	private KupovinaNarudzbinaDtoToVino kupovinaNarudzbinaDtoToVino;
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<VinoDto>> getAll(
			@RequestParam(required=false) String ime, 
			@RequestParam(required=false) Long idVinarije,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/vina?pageNo=0 ili neki drugi broj umesto 0

		Page<Vino> page = vinoService.find(ime, idVinarije, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<VinoDto> getOne(@PathVariable Long id) {
		Vino vino = vinoService.findOneById(id); 

		if (vino != null) {
			return new ResponseEntity<>(toDto.convert(vino), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VinoDto> create(@Valid @RequestBody VinoDto vinoDto) { 
		Vino vino = toVino.convert(vinoDto);

		Vino sacuvanoVino = vinoService.save(vino);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VinoDto> update(@PathVariable Long id, @Valid @RequestBody VinoDto vinoDto) { 

		if (!id.equals(vinoDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (vinoService.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Vino vino = toVino.convert(vinoDto); 

		Vino sacuvanoVino = vinoService.update(vino);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}/naruci", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VinoDto> order(@PathVariable Long id, @Valid @RequestBody KupovinaNaruzbinaDto dto) {

		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (vinoService.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		int brojNarucenihVina = dto.getBrojZaPromenu();
		Vino vino = kupovinaNarudzbinaDtoToVino.convert(dto);

		Vino sacuvanoVino = vinoService.naruciVino(vino, brojNarucenihVina);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('KORISNIK')")
	@PutMapping(value = "/{id}/kupi", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VinoDto> buy(@PathVariable Long id, @Valid @RequestBody KupovinaNaruzbinaDto dto) {

		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (vinoService.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		int brojKupljenihVina = dto.getBrojZaPromenu();

		Vino vino = kupovinaNarudzbinaDtoToVino.convert(dto);

		if (brojKupljenihVina > vino.getBrojDostupnihFlasa()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Vino sacuvanoVino = vinoService.kupiVino(vino, brojKupljenihVina);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Vino obrisanoVino = vinoService.findOneById(id);

		if (obrisanoVino != null) {
			vinoService.delete(obrisanoVino.getId());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
