package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Vino;

public interface VinoService {
	
	Vino findOneById(Long id);

	Vino save( Vino vino); 

	Vino update(Vino vino);

	Vino delete(Long id);

	Page<Vino> findAll(Integer pageNo);

	List<Vino> findAll();
	
	Page<Vino> find(String ime,  Long vinarijaId, Integer pageNo);
	
	Vino naruciVino(Vino vino, int brojNarucenih); // kada admin naruci
	
	Vino kupiVino(Vino vino, int brojKupljenih); // kada kupac kupi vino

}
