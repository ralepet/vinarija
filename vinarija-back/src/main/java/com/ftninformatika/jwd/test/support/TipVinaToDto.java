package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.TipVinaDto;
import com.ftninformatika.jwd.test.model.TipVina;

@Component
public class TipVinaToDto implements Converter<TipVina, TipVinaDto>{

	@Override
	public TipVinaDto convert(TipVina source) {
		TipVinaDto dto = new TipVinaDto();
		dto.setId(source.getId());
		dto.setIme(source.getIme());

		return dto;
	}
	
	public List<TipVinaDto> convert(List<TipVina>list){
		List<TipVinaDto>dto = new ArrayList<TipVinaDto>();
		for(TipVina tipVina : list) {   
			dto.add(convert(tipVina)); 
		}
		return dto;
	}

}
