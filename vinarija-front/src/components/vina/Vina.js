import React from 'react';
import TestAxios from './../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Vina extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            vina: [], 
            pretraga: { 
                naziv: "",
                vinarijaId: ""
            },
            currentPage:0, 
            totalPages:0,
            vinarije: [],
            brojZaPromenu :0
        }
    }

    componentDidMount() {
        this.getVina(0);
        this.getVinarije();
    }

    // TODO: Dobaviti filmove
    async getVinarije(){
        try{
            let result = await TestAxios.get("/vinarije");
            let vinarije = result.data; 
            this.setState({vinarije: vinarije});
            console.log("Vinarije su uspesno dobavljene");
        }catch(error){
            console.log(error);
            alert("Vinarije nisu dobavljene");
        } 
    }

    async getVina(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                ime: this.state.pretraga.naziv,
                idVinarije: this.state.pretraga.vinarijaId
            }
        }
        try {
            let result = await TestAxios.get('/vina', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
              vina: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }

    async naruci(idVina) {
        var params = {
            'id': idVina,
            'brojZaPromenu': this.state.brojZaPromenu
        };

        try {
            let result = await TestAxios.put('/vina/' + idVina + '/naruci', params);
            console.log(result);
            //alert('Vina je uspesno naruceno!');
            window.location.reload();
          } catch (error) {
            // handle error
            console.log(error);
            alert('Vina nisu uspesno narucena!');
          }
    }

    async kupi(idVina, brojDostupnihFlasa) {
        var params = {
            'id': idVina,
            'brojZaPromenu': this.state.brojZaPromenu
        };

        if(brojDostupnihFlasa >= this.state.brojZaPromenu){
            try {
                let result = await TestAxios.put('/vina/' + idVina + '/kupi', params);
                console.log(result);
                //alert('Vina su uspesno kupljena!');
                window.location.reload();
              } catch (error) {
                // handle error
                console.log(error);
                alert('Vina nisu uspesno narucena!');
              }
        } else{
            alert('Ne mozete naruciti vise vina nego sto ih ima na stanju!');
        }

    }

    idiNaKreiranje() {
        this.props.navigate('/vina/kreiraj');
    }

    obrisi(vinoId) {
        TestAxios.delete('/vina/' + vinoId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Vino je uspesno obrisano!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getVina(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ //
        this.getVina(this.state.currentPage+1) 
    }

    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })
    }

    brojVinaChange(event) {
        const value = event.target.value

        this.setState({ brojZaPromenu: value })
    }

    
    renderVina() {
        return this.state.vina.map((vino, index) => {
            return (
                <tr key={vino.id}>
                    <td>{vino.ime}</td>
                    <td>{vino.imeVinarije}</td>
                    <td>{vino.imeTipaVina}</td>
                    <td>{vino.opisVina}</td>
                    <td>{vino.godinaProizvodnje}</td>
                    <td>{vino.cenaFlase}</td>
                    <td>{vino.brojDostupnihFlasa}</td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(vino.id)}>Obrisi</Button></td>
                        : <td></td>}
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" && vino.brojDostupnihFlasa <= 10 ?
                        [<td><Form.Control name="brojZaPromenu" type="text" onChange={(e) => this.brojVinaChange(e)}></Form.Control></td>,
                        <td><Button variant="primary" onClick={()=>this.naruci(vino.id)}>Naruci</Button></td>
                        ] : null } 

                    {window.localStorage.getItem("role") === "ROLE_KORISNIK" ?
                        [<td><Form.Control name="brojZaPromenu" type="text" onChange={(e) => this.brojVinaChange(e)}></Form.Control></td>,
                        <td><Button variant="primary" onClick={()=>this.kupi(vino.id, vino.brojDostupnihFlasa )}>Kupi</Button></td>
                    ]: <td></td>}
                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Vina</h1></Row>
                <>
                    <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />
                </>
                <Form hidden={this.state.checked} style={{ width: "100%" }}>                    
                   
                    <Form.Label>Vinarija</Form.Label> 
                    <Form.Control as="select"  name="vinarijaId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi vinariju</option>
                        {
                            this.state.vinarije.map((vinarija) => {
                                return (
                                    <option key={vinarija.id} value={vinarija.id}>{vinarija.ime}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Form.Group>
                        <Form.Label>Naziv vina</Form.Label>
                        <Form.Control name="naziv" type="text" placeholder="Naziv vina" onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.getVina(0)}>Pretraga</Button>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="success" onClick={() => this.idiNaKreiranje()}>Kreiraj vino</Button> : null
                    }
                    <br /><br />
                <Button variant="info" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="info" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv vina</th>
                                <th>Vinarija</th> 
                                <th>Tip</th>
                                <th>Opis</th> 
                                <th>Godina proizvodnje</th>
                                <th>Cena po flasi</th>
                                <th>Broj preostalih flasa</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderVina()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Vina));