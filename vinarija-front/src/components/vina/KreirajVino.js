import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from './../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajVino extends React.Component {

    constructor(props){
        super(props);

        let vino = {
            godinaProizvodnje: "",
            cena: "",
            naziv: "" ,
            opis: "", 
            idTipaVina: "",
        }

        this.state = {
           
            vino: vino,
            tipoviVina: []
        }

    }

    componentDidMount(){
        this.getTipoveVina();
    }

    // TODO: Dobaviti filmove
    async getTipoveVina(){
        try{
            let result = await TestAxios.get("/tipovi_vina");
            let tipoviVina = result.data;
            this.setState({tipoviVina: tipoviVina});
            console.log("uspesno su dobavljeni tipovi vina");
            //alert("uspesno su dobavljeni tipovi vina");
        }catch(error){
            console.log(error);
            alert("Tipovi vina nisu uspesno dobavljeni");
        }
    }

    async kreiraj(e){ 
        e.preventDefault();

        try{
            let vino = this.state.vino;
            console.log(vino)

            let vinoDto = {
               
                ime: vino.naziv,
                opisVina: vino.opis,
                godinaProizvodnje: vino.godinaProizvodnje,
                cenaFlase: vino.cena,
                idTipaVina: vino.idTipaVina
            }
            await TestAxios.post("/vina", vinoDto);
            //let response = await TestAxios.post("/vina", vinoDto);
            this.props.navigate("/vina");
        }catch(error){
            alert("Vino nije kreirano");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let vino = this.state.vino;
        vino[name] = value;
    
        this.setState({ vino: vino });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novo vino</h1>
                <Form>
                  <Form.Label htmlFor="naziv">Naziv vina</Form.Label>
                  <Form.Control
                    id="naziv"
                    type="text"
                    placeholder="Naziv vina"
                    name="naziv"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label htmlFor="opis">Opis vina</Form.Label>
                  <Form.Control as="textarea"
                    id="opis"
                    name="opis"
                    placeholder="Opis vina"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                    <Form.Label htmlFor="godinaProizvodnje">Godina proizvodnje</Form.Label>
                    <Form.Control
                    id="godinaProizvodnje"
                    type="text"
                    name="godinaProizvodnje"
                    placeholder="Godina proizvodnje"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label htmlFor="cena">Cena po flasi</Form.Label>
                    <Form.Control
                        id="cena"
                        type="text"
                        name="cena"
                        placeholder="Cena po flasi"
                        onChange={(e) => this.valueInputChanged(e)}
                     />

                    <Form.Label htmlFor="idTipaVina">Tip vina</Form.Label> 
                    <Form.Control as="select" id="idTipaVina" name="idTipaVina" onChange={event => this.valueInputChanged(event)}>
                        <option>Izaberi tip vina</option>
                        {
                            this.state.tipoviVina.map((tipVina) => {
                                return (
                                    <option key={tipVina.id} value={tipVina.id}>{tipVina.ime}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.kreiraj(event);}}>
                    Kreiraj vino
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajVino);